try:
    tempF = float(input("Skriv inn et tall i Fahrenheit: "))
    print("Temperaturen er %.2f\u00b0F" % tempF)
    tempC = (tempF - 32) * 5/9
    print("Temperaturen er %.2f\u00b0C" % tempC)
except ValueError:
    print("Det er ikke et tall!")
