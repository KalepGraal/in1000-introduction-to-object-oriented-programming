# 1. Dette er ikke lovlig kode, da man ikke kan concatinere en int og en String.
# 2. Dersom bruker ikke skriver inn et tall, men en tekst, vil det
#    ikke kunne konverteres til int, og gi en run-time error.
#    Dersom linje 10 kjøres, vil koden gi en error, pga int og String ikke kan
#    concattes.

a = input("Tast inn et heltall! ")
b = (int(a))
if b < 10:
    print(b + "Hei!")
