from rorbu import Rorbu
from gjest import Gjest


def hovedprogram():
    rorbua = Rorbu()
    for i in range(100):
        rorbua.nyGjest(Gjest())
    rorbua.fortellVits(200)
    print(rorbua.hvorMorsomtHarViDet())
    rorbua.fortellVits(1000)
    print(rorbua.hvorMorsomtHarViDet())

hovedprogram()