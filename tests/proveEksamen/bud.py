class Bud:
    def __init__(self, budgiver, budStr):
        self._budgiver = budgiver
        if budStr <= 0:
            self._budStr = 1
        else:
            self._budStr = budStr

    def hentBudgiver(self):
        return self._budgiver

    def hentBudStr(self):
        return self._budStr
