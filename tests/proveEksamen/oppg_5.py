def arverekke(forfader, etterkommer, forstefodte):
    """
    2) Dersom forelder og førstefødte barn har samme navn, vil koden føre til en
    uendelig loop. Dersom flere av personene har samme navn, men som ikke er
    forelder og førstefødte barn, vil kun en av disse personene bli telt med,
    ettersom en ordbok kun kan ha en definisjon per nøkkel.
    """
    arverekken = []
    navn = forfader
    while navn in forstefodte and navn != etterkommer:
        arverekken.append(navn)
        navn = forstefodte[navn]
        if navn == etterkommer:
            arverekken.append(navn)
    if etterkommer in arverekken:
        return arverekken
    else:
        return []

barn = {"Halfdan":"ER", "Christian":"Hans", "Harald":"Eirik", "ER":"Harald"}
personer = arverekke("Halfdan", "Eirik", barn)
print(personer)
