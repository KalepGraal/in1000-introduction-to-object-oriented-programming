from bruktmarked import Bruktmarked

def hovedprogram():
    bruktmarked = Bruktmarked()
    a = bruktmarked.nyKategori("sykkellykt").nyAnnonse("New Yorker sykkellykt")
    a.giBud("Peter", 42)
    a.giBud("Ann", 0)
    a.giBud("CF", 32)
    a.giBud("CF", 43)
    a.giBud("CF", 32)
    a.kraftBud("Mary", 40, 50)
    a.giBud("CF", 32)

    print(a.hoyesteBud().hentBudgiver())
    print(a.hoyesteBud().hentBudStr())
    print("Lowballs:", bruktmarked.tellLaveBud())
    # assert a.hoyesteBud().hentBudgiver() == "Mary"
    # assert a.hoyesteBud().hentBudStr() == 43



hovedprogram()
