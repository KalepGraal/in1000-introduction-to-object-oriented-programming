def sjekkVerdier(tallene, min, max):
    innenfor = True
    for i in tallene:
        if i <= min or i >= max:
            innenfor = False
    return innenfor

# Hvis min > max, vil metoden alltid returnere False.

print(sjekkVerdier([0,1,3,5], -1, 10)) # True
print(sjekkVerdier([0,1,3,5], 10, -1)) # False
print(sjekkVerdier([0,1,3,5], 0, 10)) # False
