from kategori import Kategori

class Bruktmarked:
    def __init__(self):
        self._kategorier = {}

    def nyKategori(self, katNavn):
        if katNavn in self._kategorier:
            return None
        self._kategorier[katNavn] = Kategori(katNavn)
        return self._kategorier[katNavn]

    def finnKategori(self, katNavn):
        if katNavn in self._kategorier:
            return self._kategorier[katNavn]
        else:
            return None

    def tellLaveBud(self):
        laveBud = 0
        for kategori in self._kategorier:
            for annonse in self._kategorier[kategori].hentAnnonser():
                bud_paa_annonse = annonse.hentBud()
                foreløbig_hoyeste_bud = bud_paa_annonse[0]
                for bud in bud_paa_annonse:
                    if bud.hentBudStr() < foreløbig_hoyeste_bud.hentBudStr():
                        laveBud += 1
                    else:
                        foreløbig_hoyeste_bud = bud
        return laveBud
