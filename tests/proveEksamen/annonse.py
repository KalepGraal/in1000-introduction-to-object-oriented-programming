from bud import Bud

class Annonse:
    def __init__(self, annTekst):
        self._annTekst = annTekst
        self._budListe = []

    def hentTekst(self):
        return self._annTekst

    def giBud(self, hvem, belop):
        self._budListe.append(Bud(hvem, belop))


    def antBud(self):
        return len(self._budListe)

    def hoyesteBud(self):
        hoyeste = self._budListe[0]
        for bud in self._budListe:
            if hoyeste.hentBudStr() < bud.hentBudStr():
                hoyeste = bud
        return hoyeste

    # 4e
    def kraftBud(self, hvem, belop, maks):
        if belop > self.hoyesteBud().hentBudStr():
            self.giBud(hvem, belop)
        elif self.hoyesteBud().hentBudStr() + 1 <= maks:
            self.giBud(hvem, self.hoyesteBud().hentBudStr() + 1)

    # 4g
    def hentBud(self):
        return self._budListe
