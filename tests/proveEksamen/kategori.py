from annonse import Annonse

class Kategori:
    def __init__(self, katNavn):
        self._katNavn = katNavn
        self._annonser = []

    def nyAnnonse(self, annTekst):
        self._annonser.append(Annonse(annTekst))
        return self._annonser[-1]

    def hentAnnonser(self):
        return self._annonser
