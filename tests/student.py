class Student:
    def __init__(self, navn):
        self._navn = navn
        self._fagListe = []

    def leggTilFag(self, fag):
        self._fagListe.append(fag)

    def hentAntallFag(self):
        return len(self._fagListe)

    def hentStudentNavn(self):
        return self._navn

    def skrivFagPaaStudent(self):
        print("Student:", self.hentStudentNavn())
        print("     Fag:")
        for i in self._fagListe:
            print("     ", i.hentFagNavn())

