from student import Student
from fag import Fag

def hovedprogram():
    cf = Student("CF")
    asdf = Student("asdf")
    qwerty = Student("qwerty")

    IN1000 = Fag("IN1000")
    IN1020 = Fag("IN1020")
    IN1050 = Fag("IN1050")
    
    IN1000.leggTilStudent(cf)
    IN1000.leggTilStudent(asdf)
    IN1000.leggTilStudent(qwerty)

    cf.leggTilFag(IN1020)
    cf.leggTilFag(IN1050)
    
    cf.skrivFagPaaStudent()
    print("\n")
    IN1000.skrivStudenterVedFag()

hovedprogram()
