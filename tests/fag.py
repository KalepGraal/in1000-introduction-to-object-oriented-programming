class Fag:
    def __init__(self, navn):
        self._navn = navn
        self._studentListe = []

    def leggTilStudent(self, student):
        self._studentListe.append(student)

    def hentAntallStudenter(self):
        return len(self._studentListe)

    def hentFagNavn(self):
        return self._navn

    def skrivStudenterVedFag(self):
        print("Fag:", self.hentFagNavn())
        print("     Studenter")
        for i in self._studentListe:
            print("     ", i.hentStudentNavn())

