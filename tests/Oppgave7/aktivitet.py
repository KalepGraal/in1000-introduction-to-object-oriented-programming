class Aktivitet:
    def __init__(self, hva, kl):
        self._aktNavn = hva
        self._start = kl

    def hentNavn(self):
        return self._aktNavn

    def hentStart(self):
        return self._start
