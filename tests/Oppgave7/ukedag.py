from aktivitet import Aktivitet


class Ukedag:
    def __init__(self, dag):
        self._dag = dag
        self._timeplan = {}

    def settInn(self, hva, kl):
        if kl not in self._timeplan:
            self._timeplan[kl] = Aktivitet(hva, kl)
        else:
            print("Feil, kl:", kl, "er allerede i timeplanen")

    def settInnLedig(self, hva):
        if len(self._timeplan) == 24:
            print("Feil: Timeplanen er full!")
        elif len(self._timeplan) == 0:
            self._timeplan[12] = Aktivitet(hva, 12)
        else:
            tidligste = self.tidligste()
            seneste = self.seneste()

            counter = tidligste
            while counter < seneste:
                if counter not in self._timeplan:
                    self._timeplan[counter] = Aktivitet(hva, counter)
                    counter = seneste
                else:
                    counter += 1

            if seneste < 24:
                self._timeplan[seneste+1] = Aktivitet(hva, seneste+1)
            elif tidligste > 0:
                self._timeplan[tidligste-1] = Aktivitet(hva, tidligste-1)

    def tidligste(self):
        """:returns int"""
        if len(self._timeplan) == 0:
            return -1
        tidligste = 24
        for i in self._timeplan:
            if i < tidligste:
                tidligste = i
        return tidligste

    def seneste(self):
        if len(self._timeplan) == 0:
            return -1
        seneste = 0
        for i in self._timeplan:
            if i > seneste:
                seneste = i
        return seneste

    def antall(self):
        return len(self._timeplan)
