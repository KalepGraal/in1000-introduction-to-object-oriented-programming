class Gjest:
    def __init__(self):
        self._underholdningsverdi = 0

    def underhold(self, verdiøkning):
        self._underholdningsverdi += verdiøkning

    def hentUnderholdningsverdi(self):
        return self._underholdningsverdi
