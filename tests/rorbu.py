class Rorbu:
    def __init__(self):
        self._gjester = []

    def nyGjest(self, gjest):
        for i in self._gjester:
            i.underhold(1)
        self._gjester.append(gjest)

    def fortellVits(self, tall):
        for i in self._gjester:
            i.underhold(tall)

    def hvorMorsomtHarViDet(self):
        tot = 0
        for i in self._gjester:
            tot += i.hentUnderholdningsverdi()
        avg = tot / len(self._gjester)

        if avg < 200:
            return "Kjedelig kveld"
        elif avg < 400:
            return "Dette var jo litt gøy"
        elif avg < 600:
            return "Dette var artig!"
        else:
            return "Dra på Lopphavet - bi dæ godtar no så gyt e!"

    def hentAntallGjester(self):
        return len(self._gjester)