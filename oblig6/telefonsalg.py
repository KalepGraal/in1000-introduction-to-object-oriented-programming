# Henter ut informasjon fra "salgstall.txt", og printer dette ut. 

# Tar et filnavn som parameter, og lager en ordbok hvor det første ordet i hver
# linje i filnavn er nøkkel, og tallet etterpå er verdien. formatet: "Key Value"
# Returnerer ordbok
def innlesing(filnavn):
    info = {}
    with open(filnavn) as f:
        for line in f:
            lineInfo = line.split()
            navn = lineInfo[0]
            salg = int(lineInfo[1])
            info[navn] = salg
    return info

# Finner den nøkkelen i en ordbok som har høyest verdi, printer dette ut.
def maanedensSalgsperson(ordbok):
    maks = 0
    for i in ordbok:
        if ordbok[i] > maks:
            maks = ordbok[i]
            navn = i

    print("Den som solgte mest denne måneden var %s med %i salg!" % (navn, maks))

# Finner det totale antallet salg i en ordbok. Returnerer dette.
def totaltAntallSalg(ordbok):
    total = 0
    for i in ordbok:
        total += ordbok[i]
    return total

# Finner gjennomsnittet en selger solgte denne måneden
def gjennomsnittSalg(ordbok):
    totalSalg = totaltAntallSalg(ordbok)
    antallSelgere = len(ordbok)
    gjennomsnitt = totalSalg / antallSelgere
    return gjennomsnitt

# Definerer hovedprogrammet
def hovedprogram():
    # lager en ordbok fra filen "salgstall.txt"
    salgstall = innlesing("salgstall.txt")
    # Sjekker hvem i ordboken salgstall, som solgte mest denne måneden.
    maanedensSalgsperson(salgstall)
    # Printer antallet aktive selgere, totale antallet salg,
    # og gjennomsnittlige salg per selger denner måned.
    print("Aktive selgere denne måneden: %i" % len(salgstall))
    print("Totalt antall salg: %i" % totaltAntallSalg(salgstall))
    print("Gjennomsnittlig antall salg per salgsperson: %i" % gjennomsnittSalg(salgstall))

# Kjører hovedprogrammet
hovedprogram()
