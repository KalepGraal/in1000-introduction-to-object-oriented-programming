# Tester klassen Person

# importerer klassen Person fra person.py
from person import Person

# Definerer en funksjon lagPerson. Den tar brukerinput navn og alder,
# og spør om hobbyer. Lager et objekt av klassen Person med denne infoen.
# returnerer et objekt av klassen Person.
def lagPerson():
    print("Lager en ny person.")
    navn = input("Navn: ")
    alder = int(input("Alder: "))
    person = Person(navn, alder)    # Lager objektet, med navn og alder
    # Legger til hobbyer i objektets hobbyer.
    print("Legg til hobbyer:")
    hobby = input("Hobby (0 to quit): ")
    while hobby != "0":
        person.leggTilHobby(hobby)
        hobby = input("Hobby (0 to quit): ")
    # Skriver ut info om objektet
    print("\nInfo om din person:")
    person.skrivUt()
    # returner objektet
    return person

# Lager hovedprogrammet.
def hovedprogram():
    # Lager et testobjekt, per, og legger til tre hobbyer.
    per = Person("Per", 47)
    per.leggTilHobby("Fisking")
    per.leggTilHobby("Gambling")
    per.leggTilHobby("Netflix")
    # Skriver ut informasjonen i per.
    per.skrivUt()
    # Lager en ny Person, ole, med funksjonen lagPerson
    ole = lagPerson()

# Kjører hovedprogrammet
hovedprogram()
