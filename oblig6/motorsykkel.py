# Definerer klassen motorsykkel
class Motorsykkel:
    # Lager konstruktøren, tar variablene merke, regNum og kmKjort som parametre
    def __init__(self, merke, regNum, kmKjort):
        # Lager instansvariabler fra parametrene
        self._merke = merke
        self._regNum = regNum
        self._kmKjort = kmKjort

    # Lager metoden kjor, som tar km som parameter, og øker _kmKjort med km
    def kjor(self, km):
        self._kmKjort += km

    # Returnerer _kmKjort
    def hentKilometerstand(self):
        return self._kmKjort

    # Skriver ut _merke, _regNum og _kmKjort for det objektet som kaller metoden
    def skrivUt(self):
        # skriv ut merke, regNum, kmKjort
        print("Merke:", self._merke)
        print("Registreringsnummer:", self._regNum)
        print("Kilometerstand:", self._kmKjort)
