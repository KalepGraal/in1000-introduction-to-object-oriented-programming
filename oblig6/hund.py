# Definerer klassen Hund
class Hund:
    # Lager konstruktøren til klassen.
    # Klassen har tre instansvariabler: alder, vekt og metthet.
    def __init__(self, alder, vekt):
        self._alder = alder
        self._vekt = vekt
        self._metthet = 10

    # Definerer metoder som henter verdien i alder, og i vekt
    def getAlder(self):
        return self._alder
    def getVekt(self):
        return self._vekt

    # Endrer _vekt dersom _metthet er mindre enn 5.
    def spring(self):
        self._metthet -= 1
        if self._metthet < 5:
            self._vekt -= 1

    # Øker vekt med 1, dersom metthet er over 7.
    def spis(self, tall):
        self._metthet += tall
        if self._metthet > 7:
            self._vekt += 1
