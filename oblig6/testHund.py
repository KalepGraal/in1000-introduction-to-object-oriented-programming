# Tester klassen Hund. 

# Importerer klassen Hund fra hund.py
from hund import Hund

# Definerer hovedprogrammet
def hovedprogram():
    # Lager en ny Hund, doginator, med alder 6, og vekt 20
    doginator = Hund(6, 20)
    # Kaller metoden spring på doginator, og printer vekt
    doginator.spring()
    print("Vekt =", doginator.getVekt())
    # Kaller metoden spis, med verdien 5, og printer vekt
    doginator.spis(5)
    print("Vekt =", doginator.getVekt())

    # Kaller metoden spring på doginator, og printer vekt
    doginator.spring()
    print("Vekt =", doginator.getVekt())
    # Kaller metoden spis, med verdien 10, og printer vekt
    doginator.spis(10)
    print("Vekt =", doginator.getVekt())

# Kjører hovedprogrammet
hovedprogram()
