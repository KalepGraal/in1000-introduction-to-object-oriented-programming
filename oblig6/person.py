# Oppgave 5, bruker oppgavetekstens forslag til oppgave.

# Definerer klassen Person.
class Person:
    # Lager konstruktøren til klassen.
    # Klassen har tre instansvariabler: navn, alder, og listen hobbyer
    def __init__(self, navn, alder):
        self._navn = navn
        self._alder = alder
        self._hobbyer = []

    # Tar en parameter, hobby, og legger den til i lista hobbyer
    def leggTilHobby(self, hobby):
        self._hobbyer.append(hobby)

    # Printer ut hobbyer i en liste.
    def skrivHobbyer(self):
        hobbyer = self._hobbyer
        utskrift = ""
        for i in range(len(hobbyer)):
            if i == len(hobbyer) -1:
                utskrift += hobbyer[i]
            else:
                utskrift += hobbyer[i] + ", "

        print("Hobbyer:", utskrift)

    # Printer ut navn, alder, og listen fra skrivHobbyer
    def skrivUt(self):
        print("Navn:", self._navn)
        print("Alder:", self._alder)
        self.skrivHobbyer()
