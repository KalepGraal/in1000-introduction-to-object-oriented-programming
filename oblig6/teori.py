#    1. Hva er innkapsling? Hvorfor er det nyttig?
Innkapsling er det at man lager et grensesnitt til koden, slik at man ikke
trenger å ta hensyn til implementasjonen av de forskjellige metodene. Dette vil
si at vi vet hva de forskjellige metodene gjør, uten å vite hvordan de er
implementert.
#    2. Hva er grensesnittet til en klasse? Hvordan skiller det seg fra
#       implementasjonen av en klasse?
Grensesnittet til en klasse er de forskjellige metodene til klassen, samt en
beskrivelse av hva de gjør.
#    3. Hva er en instansmetode, og hvordan skiller dette seg fra
#       prosedyrene/funksjonene vi har møtt hittil?
En instansmetode er en metode inne i klassen som tar self som første parameter.
self er navnet på objektet som metoden blir kalt på. Vi skriver ikke inn
objektnavnet som en parameter, da vi skriver objektnavnet i selve metodekallet.
