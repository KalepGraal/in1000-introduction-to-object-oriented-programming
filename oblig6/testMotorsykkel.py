# Tester klassen Motorsykkel

# Importerer klassen Motorsykkel fra motorsykkel.py
from motorsykkel import Motorsykkel

# Lager hovedprogrammet
def hovedprogram():
    # Lager tre objekter (m1, m2, m3) av klassen Motorsykkel.
    m1 = Motorsykkel("BMW", "AF-4253", 6404)
    m2 = Motorsykkel("Yamaha", "QR-4625", 4530034)
    m3 = Motorsykkel("Hyundai", "CF-5323", 42)

    # Skriver ut instansvariablene til m1
    print("m1")
    m1.skrivUt()
    # Skriver ut instansvariablene til m2
    print("\nm2")
    m2.skrivUt()
    # Skriver ut instansvariablene til m3
    print("\nm3")
    m3.skrivUt()

    # Kaller metoden kjor(10) på m3, dette øker kilometerstanden til m3 med 10
    m3.kjor(10)
    print("\nOppdatert kilometerstand:", m3.hentKilometerstand())

hovedprogram()
