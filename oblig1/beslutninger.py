# Spør brukeren om et svar, sørger for at svaret er lowercase
svar = input("Vil du ha en brus? (ja/nei): ").lower()

# Sjekker om brukeren har svart ja
if svar == "ja":
    print("Her har du en brus!")
# Dersom brukeren ikke svarte ja, sjekker om brukeren har svart nei
elif svar == "nei":
    print("Den er grei.")
# Dersom brukeren ikke har svart ja eller nei, printer at den ikke forstår
else:
    print("Det forstod jeg ikke helt.")
