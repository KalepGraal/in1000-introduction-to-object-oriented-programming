# lager 2 heltallsvariabler og printer de ut til konsollen
var1 = 30
var2 = 12
print(var1)
print(var2)

# legger variablene sammen, og printer de ut til konsollen
sum = var1 + var2
print("Sum: %i" % sum)

# lager to strengvariabler
strVar1 = "Hello"
strVar2 = "World"

# legger strengvariablene sammen, og printer de ut
sammen = strVar1 + strVar2
sammen = strVar1 + " " + strVar2
print(sammen)
