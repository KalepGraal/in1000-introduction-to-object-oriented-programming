# Importerer klasser som brukes i denne filen.
from random import randint
from celle import Celle


class Spillebrett:
    """Definerer klassen Spillebrett, som har fire instansvariabler, generasjonsnummer (_genNum), _rader,
       _kolonner, og _rutenett"""
    def __init__(self, rader, kolonner):
        # Konstruerer klassen, tar to parametre: rader og kolonner.
        self._genNum = 0

        # Bruker parametrene til aa definere instansvariablene _rader og _kolonner
        self._rader = rader
        self._kolonner = kolonner

        # Lager et rutenett (_rutenett). Lager foerst en liste, bruker saa prosedyren lagRutenett() til aa
        # lage rutenettet. Gjoer saa et tilfeldig antall av cellene levende med generer().
        self._rutenett = []
        self.lagRutenett(self._rader, self._kolonner)
        self.generer()

    def lagRutenett(self, rader, kolonner):
        """Lager en noestet liste ut ifra antallet rader og kolonner"""
        for i in range(rader):
            self._rutenett.append([])
            for j in range(kolonner):
                self._rutenett[i].append(Celle())

    def tegnBrett(self):
        """Printer ut brettet"""
        print("\n"*30)
        for i in range(self._rader):
            for j in range(self._kolonner):
                print(self._rutenett[i][j].tegnStatus(), end="")
            print()

    def oppdatering(self):
        """Lager neste generasjon med celler, ut ifra reglene som ble gitt"""
        bringToLife = []
        killList = []

        # Bruker her prosedyren sjekkOmCellenSkalLeve til aa finne ut hvilke celler som skal
        # doe, og hvilke som skal bli levende.
        self.sjekkOmCellenSkalLeve(bringToLife, killList)

        # Gjoer cellene, som skal bli levende, levende.
        for i in bringToLife:
            i.settLevende()

        # Gjoer cellene, som skal bli doede, doede.
        for i in killList:
            i.settDoed()

        # Oeker generasjonsnummeret
        self._genNum += 1

    def sjekkOmCellenSkalLeve(self, revive, kill):
        """ Sjekker hver celle mot reglene, og legger de til i en av to lister, dersom
            dersom de skal endres paa. """
        for i in range(self._rader):
            for j in range(self._kolonner):
                # Kjoerer for hver celle i _rutenett:
                cellen = self._rutenett[i][j]  # Cellen som sjekkes i denne iterasjonen av loekken

                # Lager en liste med de levende naboene til cellen.
                levendeNaboer = self.finnLevendeNabo(i, j)

                # Planlegger aa drepe cellen dersom den er levende,
                # og har < 2 levendenaboer eller > 3 levende naboer
                if cellen.erLevende():
                    if len(levendeNaboer) < 2:  # Underpopulasjon
                        kill.append(cellen)
                    elif len(levendeNaboer) > 3:    # Overpopulasjon
                        kill.append(cellen)
                # Planlegger aa opplive cellen dersom den er doed,
                # og har akkurat 3 levende naboer
                else:
                    if len(levendeNaboer) == 3:
                        revive.append(cellen)

    def finnAntallLevende(self):
        """Finner det totale antallet levende celler, og returnerer det."""
        antallLevende = 0
        for i in self._rutenett:
            for j in i:
                if j.erLevende():
                    antallLevende += 1
        return antallLevende

    def finnGenNum(self):
        """Returnerer generasjonsnummeret som brettet er paa for oeyeblikket"""
        return self._genNum

    def generer(self):
        """Setter et tilfeldig antall (Ca 25%) av cellene, levende."""
        for i in range(self._rader):
            for j in range(self._kolonner):
                rand = randint(0, 3)
                if rand == 3:
                    self._rutenett[i][j].settLevende()

    def finnLevendeNabo(self, x, y):
        """Finner de levende naboene til et koordinat, og returnerer de i en liste"""
        levendeNaboer = []
        for i in range(-1, 2):
            for j in range(-1, 2):
                naboX = x + i
                naboY = y + j
                if not (naboX == x and naboY == y):
                    if not (naboX < 0 or naboY < 0 or naboX > self._rader - 1 or naboY > self._kolonner - 1):
                        if self._rutenett[naboX][naboY].erLevende():  # Dersom denne naboen er levende
                            levendeNaboer.append(self._rutenett[naboX][naboY])
        return levendeNaboer
