# Importerer klasser som brukes i denne filen.
from spillebrett import Spillebrett
from time import sleep


def hovedprogram():
    """Hovedprogrammet"""
    skrivTittel()  # Skriver ut tittel, en linje om gangen.
    # Lager et nyt brett fra brukerinput med funksjonen lagBrettFraBrukerInput()
    spillebrett = lagBrettFraBrukerInput()
    # Starer gameOfLife paa spillebrett
    gameOfLife(spillebrett)


def lagBrettFraBrukerInput():
    """Lager et Spillebrett, basert på input fra bruker"""
    rader = int(input("Antall rader: "))
    kolonner = int(input("Antall kolonner: "))
    return Spillebrett(rader, kolonner)


def gameOfLife(brett):
    """Skriver ut brettet. Lager neste generasjons brett, dersom brukerinput er "".
    Avsluttes dersom brukerinput "q". Dersom brukerinput er "l", lager den og skriver ut
    de neste 10 brettene i intervaller paa 0.1s. """
    running = ""
    while running != "q":
        if running == "l":
            for i in range(10):
                kjoer(brett)
                sleep(0.1)
        else:
            kjoer(brett)
        running = hentBrukerInput()
        if running == "q":
            print("\nBye!\n")


def hentBrukerInput():
    """Henter brukerinput, og returnere det, saa lenge det er en av følgende: "", "l", "q".
    Dersom brukerinputten er noe annet, vil den spoerre om input paa nytt."""

    brukerInput = input("Press enter for aa fortsettte. Skriv in \"l\" for aa kjoere 10 ganger. \n"
                        "Skriv inn \"q\" og trykk enter for aa avslutte: \n")
    if brukerInput not in ("", "l", "q"):
        print("Ugyldig Input, prøv igjen. ")
        brukerInput = hentBrukerInput()
    return brukerInput


def kjoer(brett):
    """Tegner brettet, skriver ut generasjon og antall levende celler, oppdaterer saa brettet"""
    brett.tegnBrett()
    print("Generasjon %i - Antall levende celler: %i" % (brett.finnGenNum(), brett.finnAntallLevende()))
    brett.oppdatering()


def skrivTittel():
    """Printer ut hver linje i name, en linje av gangen, med intervaller paa 0.05s"""
    name = [
        "\033[34m",
        " ██████╗ ██████╗ ███╗   ██╗██╗    ██╗ █████╗ ██╗   ██╗███████╗",
        "██╔════╝██╔═══██╗████╗  ██║██║    ██║██╔══██╗╚██╗ ██╔╝██╔════╝",
        "██║     ██║   ██║██╔██╗ ██║██║ █╗ ██║███████║ ╚████╔╝ ███████╗",
        "██║     ██║   ██║██║╚██╗██║██║███╗██║██╔══██║  ╚██╔╝  ╚════██║",
        "╚██████╗╚██████╔╝██║ ╚████║╚███╔███╔╝██║  ██║   ██║   ███████║",
        " ╚═════╝ ╚═════╝ ╚═╝  ╚═══╝ ╚══╝╚══╝ ╚═╝  ╚═╝   ╚═╝   ╚══════╝",
        "",
        " ██████╗  █████╗ ███╗   ███╗███████╗     ██████╗ ███████╗    ██╗     ██╗███████╗███████╗",
        "██╔════╝ ██╔══██╗████╗ ████║██╔════╝    ██╔═══██╗██╔════╝    ██║     ██║██╔════╝██╔════╝",
        "██║  ███╗███████║██╔████╔██║█████╗      ██║   ██║█████╗      ██║     ██║█████╗  █████╗  ",
        "██║   ██║██╔══██║██║╚██╔╝██║██╔══╝      ██║   ██║██╔══╝      ██║     ██║██╔══╝  ██╔══╝  ",
        "╚██████╔╝██║  ██║██║ ╚═╝ ██║███████╗    ╚██████╔╝██║         ███████╗██║██║     ███████╗",
        " ╚═════╝ ╚═╝  ╚═╝╚═╝     ╚═╝╚══════╝     ╚═════╝ ╚═╝         ╚══════╝╚═╝╚═╝     ╚══════╝",
        "\033[0m"
    ]
    for i in name:
        print(i)
        sleep(0.05)

hovedprogram()
