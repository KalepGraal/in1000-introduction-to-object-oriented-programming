# Definerer klassen Celle, som har en instansvariabel, booleanen _isAlive,  og metoder for aa
# endre, og for aa returnere statusen på denne booleanen.


class Celle:
    def __init__(self):
        """Konstruerer cellen, default verdi for _isAlive er False"""
        self._isAlive = False

    def settDoed(self):
        """Metode for aa sette cellen doed"""
        self._isAlive = False

    def settLevende(self):
        """Metode for aa sette cellen levende"""
        self._isAlive = True

    def erLevende(self):
        """Funksjon som returnerer verdien til _isAlive"""
        return self._isAlive

    def tegnStatus(self):
        """Returnerer et tegn, basert på verdien til _isAlive.
           Bruker her firkanter med forskjellige farger,
           i steden for det som ble sagt i oppgaven"""
        if self._isAlive:
            return "\033[32m" + "██" + "\033[0m"
        else:
            return "\033[31m" + "██" + "\033[0m"
