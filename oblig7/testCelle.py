from celle import Celle
c1 = Celle()
c2 = Celle()
c3 = Celle()

c2.settLevende()

print("c1 is alive:", c1.erLevende())
print("c2 is alive:", c2.erLevende())
print("c3 is alive:", c3.erLevende())
print("c2: ", c2.tegnStatus())

c2.settDoed()

print("c1 is alive:", c1.erLevende())
print("c2 is alive:", c2.erLevende())
print("c3 is alive:", c3.erLevende())
print("---")
print("c2: ", c2.tegnStatus())