def mindreStorre(x):
    if(x > 20):
        print("Tallet er større enn 20")
    elif(x > 10):
        print("Tallet er mellom 10 og 20")
    elif(x < 10):
        print("Tallet er mindre enn 10")
    elif(x == 10):
        print("Tallet er 10")
    else:
        print("Ikke et tall")

try:
    inp = int(input("Tall plz: "))
    mindreStorre(inp)
except ValueError:
    print("Ikke et tall")
