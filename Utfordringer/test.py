def fyllTilTi(tallene):
    while len(tallene) < 10:
        tallene.append(0)
    return tallene

tall = [1, 2, 3, 4, 5]
print(fyllTilTi(tall))

talls = [0]
print(fyllTilTi(talls))
