import sys
import json


def main():
    filename = sys.argv[1]
    words_in_file = list_words(filename)
    liste = []
    for word in words_in_file:
        liste.append([word, words_in_file[word]])

    # Printer 100 mest brukte ordene i teksten
    test = sorted(liste, key=lambda word: word[1][0], reverse=True)
    for i in range(len(test)):
        if i < 100:
            print("\"%s\" occurs %i times" % (test[i][0], test[i][1][0]))

    write_to_json(test, "oppg9.json")


def list_words(filename):
    """
    :returns dictionary, format: "ord: [forekomster, [linjer]]"
    """
    with open(filename) as file:
        lines = []
        for line in file:
            line = remove_formatting(line)
            lines.append(line.strip("\n").split(" "))
        words = {}
        for line in range(len(lines)):  # for hver linje i fila:
            for word in lines[line]:  # for hvert ord i hver linje:
                if word not in words:
                    words[word] = []
                    words[word].append(line)
                else:
                    words[word].append(line)
        for word in words:
            words[word] = [len(words[word]), words[word]]
        del(words[""])  # Fjerner blanke linjer, uten å påvirke linjenummerering
        return words


def remove_formatting(line):
        # Fjerner formatering
        line = line.replace("[", "")
        line = line.replace("]", "")
        line = line.replace(".", "")
        line = line.replace(",", "")
        line = line.replace("?", "")
        line = line.replace(":", "")
        line = line.replace("!", "")
        line = line.lower()  # gjør alle bokstaver små
        return line


def write_to_json(list_to_write, json_name):
    """:param list_to_write = ord: [forekomster, [linjer]]"""
    with open(json_name, "w") as file:
        words = {}
        for i in range(len(list_to_write)):
            words[list_to_write[i][0]] = {}
            words[list_to_write[i][0]]["forekomster"] = list_to_write[i][1][0]
            words[list_to_write[i][0]]["linjer"] = []

            for j in list_to_write[i][1][1]:
                words[list_to_write[i][0]]["linjer"].append(j)

        file.write(json.dumps(words, indent=4, separators=(',', ':')))


# Kjører hovedprogrammet dersom det er oppg_9.py som kjøres i terminalen


if __name__ == "__main__":
    main()
