from rack import Rack
from node import Node


class Regneklynge:
    def __init__(self, file):
        """
        Constructor for Regneklynge.
        :param file: String, filep+ath to file used to create racks with nodes.
        :return: None
        """
        self._racks = []
        with open(file, "r") as f:
            self._max_nodes_per_rack = int(f.readline())
            self.add_nodes_from_file(f)

    def add_nodes_from_file(self, file):
        """
        Adds racks and nodes from info in a file.
        :param file: String, filepath to file with info
        :return: None
        """
        for line in file:
            info = line.split()
            for i in range(int(info[0])):
                self.add_node_to_a_rack(Node(int(info[1]), int(info[2])))

    def add_node_to_a_rack(self, node):
        """
        Adds a node to the last rack. Makes a new rack if the rack is full.
        Assumes that only the last rack can change.
        :param node: Node object.
        :return: None
        """
        if len(self._racks) == 0 or self._racks[-1].is_full():
            self._racks.append(Rack(self._max_nodes_per_rack))
            self._racks[-1].add_node(node)
        else:
            self._racks[-1].add_node(node)

    def get_total_processor_count(self):
        """
        Counts the total number of processors in this computer cluster.
        :return: int, processors on all nodes combined
        """
        processor_count = 0
        for rack in self._racks:
            nodes = rack.get_nodes()
            for node in nodes:
                processor_count += node.get_number_of_processors()
        return processor_count

    def get_total_nodes_with_enough_memory(self, required_memory):
        """
        Counts the amount of nodes with the required memory.
        :param required_memory: int, memory in GB
        :return: int, the amount of nodes with enough memory
        """
        nodes_with_enough_memory = []
        for rack in self._racks:
            nodes = rack.get_nodes()
            for node in nodes:
                if node.get_memory() >= required_memory:
                    nodes_with_enough_memory.append(node)
        return len(nodes_with_enough_memory)

    def get_rack_count(self):
        """
        :return: int, total number of racks in this cluster.
        """
        return len(self._racks)
