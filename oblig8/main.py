from regneklynge import Regneklynge


def main():
    """
    The main program.
    :return: None
    """
    abel = Regneklynge("regneklynge.txt")

    print("Nodes with atleast 32 GB:", abel.get_total_nodes_with_enough_memory(32))
    print("Nodes with atleast 64 GB:", abel.get_total_nodes_with_enough_memory(64))
    print("Nodes with atleast 128 GB:", abel.get_total_nodes_with_enough_memory(128))

    print("\nTotal processors:", abel.get_total_processor_count())
    print("Racks:", abel.get_rack_count())

main()
