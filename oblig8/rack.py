class Rack:
    def __init__(self, max_nodes_per_rack):
        """
        Constructor for a new rack.
        :param max_nodes_per_rack: maximum number of nodes in this rack.
        :return: None
        """
        self._nodes = []
        self._max_nodes_per_rack = max_nodes_per_rack

    def is_full(self):
        """
        :return: boolean, True if there is no more room in this rack
        """
        return len(self._nodes) == self._max_nodes_per_rack

    def add_node(self, node):
        """
        Adds a new node to the rack, if there is available room
        :param node: Node object.
        :return: None
        """
        if not self.is_full():  # Sjekkes også i regneklynge @addNode, fjernes?
            self._nodes.append(node)

    def get_nodes(self):
        """
        :return: list of nodes in this rack
        """
        return self._nodes
