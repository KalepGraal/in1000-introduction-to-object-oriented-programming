class Node:
    def __init__(self, memory, number_of_processors):
        """
        Constructor for a new Node.
        :param memory: int, memory in GB
        :param number_of_processors: int, number of processors this node has
        :return: None
        """
        self._memory = memory  # In GB
        self._number_of_processors = number_of_processors

    def get_number_of_processors(self):
        """
        :return: int, number of processors this node has
        """
        return self._number_of_processors

    def get_memory(self):
        """
        :return: int, memory this node has
        """
        return self._memory
