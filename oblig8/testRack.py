from rack import Rack

r1 = Rack(1)
r2 = Rack(2)
r3 = Rack(3)

r1._noder.append("!")

r2._noder.append("!")
r2._noder.append("!")

r3._noder.append("!")
r3._noder.append("!")

print("r1 is full:", r1.is_full())
print("r2 is full:", r2.is_full())
print("r3 is full:", r3.is_full())