from hytte import Hytte


class Tur:
    def __init__(self, hytter, beskrivelse):
        self._hytter = hytter
        self._beskrivelse = beskrivelse
    
    def skrivTur(self):
        print(self._beskrivelse)

        print(self._hytter)
        for hytte in self._hytter:
            hytte.skrivHytte()
    
    def sjekkPrisPlass(self, antPers, maksBelop):
        pris = 0
        nokPlass = True
        for hytte in self._hytter:
            pris += hytte.totPris(antPers)
            if not hytte.sjekkPlass(antPers):
                nokPlass = False
        if pris < maksBelop and nokPlass:
            return True
        return False
    
    # Brukes i oppgave 4e
    def hentAntHytter(self):
        return len(self._hytter)
