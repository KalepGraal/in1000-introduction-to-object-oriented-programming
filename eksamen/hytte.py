class Hytte:
    def __init__(self, navn, antSenger, pris):
        self._navn = navn
        self._antSenger = antSenger
        self._pris = pris
    
    def hentNavn(self):
        return self._navn
    
    def totPris(self, antPers):
        return self._pris * antPers
    
    def skrivHytte(self):
        print("Navn:", self._navn, ", Antallet senger:", self._antSenger, ", Pris per seng:", self._pris)
    
    def sjekkPlass(self, antall):
        if self._antSenger >= antall:
            return True
        return False

