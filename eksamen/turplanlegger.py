from hytte import Hytte
from tur import Tur


class Turplanlegger:
    def __init__(self):
        self._hytter = [Hytte("Hytte1", 7, 100), Hytte("Hytte2", 7, 100), Hytte("Hytte3", 7, 100),
                        Hytte("Hytte4", 7, 100), Hytte("Hytte5", 7, 100), Hytte("Hytte6", 7, 100)]
        self._turer = [Tur([self._hytter[0], self._hytter[1]], "beskrivelse1"),
                       Tur([self._hytter[2], self._hytter[3]], "beskrivelse2"),
                       Tur([self._hytter[4], self._hytter[5]], "beskrivelse3")]
    
    def hytterFraFil(self, fil):
        hytter = {}
        with fil as f:
            for linje in f:
                linje = linje.split()
                hytter[linje[0]] = Hytte(linje[0], int(linje[1]), int(linje[2]))
        return hytter
    
    def finnTurer(self, antPers, maksPris, maksDager):
        for tur in self._turer:
            if tur.sjekkPrisPlass(antPers, maksPris) and maksDager >= tur.hentAntHytter():
                tur.skrivTur()


