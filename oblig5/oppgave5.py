# Oppgaven: forslagsoppgaven fra oppgaveteksten.
# (velger her å bruke en orbok som input i prosedyren getSkredderInfo, i stedenfor en
# liste)

# Definerer tommerTilCm, gir AssertionError dersom input <= 0
def tommerTilCm(antallTommer):
    assert antallTommer > 0
    return antallTommer * 2.54

# Definerer printInCm(), som tar en ordbok som parameter, og skriver ut nøklene
# og de tilhørende verdiene i cm ved hjelp av tommerTilCm().
def printInCm(ordbok):
    for k, v in ordbok.items():
        print("%s: %.2fcm" % (k, tommerTilCm(v)))

# Definerer getSkredderInfo som tar et filnavn som parameter. Den vil så åpne
# filen, og lage en ordbok fra filen. Nøkkelverdiene er de første ordene på hver
# linje, mens de tilhørende verdiene er de neste elementene på linjene.
def getSkredderInfo(filename):
    info = {}
    with open(filename) as f:
        for line in f:
            lineInfo = line.split()
            key = lineInfo[0]
            value = float(lineInfo[1])
            info[key] = value
    return info

# Definerer hovedprogram
def main():
    # Lager ordboken skredderInfo ut fra filen skredderInfo.txt
    skredderInfo = getSkredderInfo("skredderInfo.txt")
    # Printer ut informasjonen fra filen i cm.
    printInCm(skredderInfo)

# Kjører hovedprogrammet.
main()
