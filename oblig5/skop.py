"""
Først defineres funksjonen min funksjon, og prosedyren hovedprogram. Deretter
vil hovedprogram kalles, og kjøres. Da settes a til 42, og b til 0, så vil
b printes ut (0). så settes b til samme verdi som a (42), og a settes til
minFunksjon().

I minFunksjon() vil løkken kjøre 2 ganger. I løkken blir c satt til 2, og
printet ut. Deretter blir c økt med 1, og blir 3. så settes b til 10. Deretter
prøver programmet å øke b med a. Da vil vi få en feilmelding, ettersom a ikke
er definert i funksjonen. Dette vil avslutte programmet.

a som blir definert i hovedprogrammet, er ikke en global variabel, men en lokal.
Dette fører til at variablen a er utenfor skopet til minFunksjon().
"""

def minFunksjon():
    for x in range(2):
        c = 2
        print(c)
        c += 1
        b = 10
        b += a
        print(b)
    return(b)
    
def hovedprogram():
    a = 42
    b = 0
    print(b)
    b = a
    a = minFunksjon()
    print(b)
    print(a)

hovedprogram()
