# Definerer addisjon og en test til addisjon
def addisjon(a, b):
    return a + b
def addisjonTest():
    assert addisjon(5,2) == 7
    assert addisjon(4, 6) == 10
    assert addisjon(1, -3) == -2
    assert addisjon(-4, -2) == -6

# Definerer subtraksjon og en test til subtraksjon
def subtraksjon(a, b):
    return a - b
def subtraksjonTest():
    assert subtraksjon(5,2) == 3
    assert subtraksjon(4, 6) == -2
    assert subtraksjon(1, -3) == 4
    assert subtraksjon(-4, -3) == -1

# Definerer divisjon og en test til divisjon
def divisjon(a, b):
    return a / b
def divisjonTest():
    assert divisjon(6,2) == 3
    assert divisjon(3, 6) == 0.5
    assert divisjon(-6, 2) == -3
    assert divisjon(-4, -2) == 2

# Definerer tommerTilCm, gir AssertionError dersom input <= 0
def tommerTilCm(antallTommer):
    assert antallTommer > 0
    return antallTommer * 2.54

# Definerer skrivBeregninger
def skrivBeregninger():
    """
    spør om to tall fra bruker, og bruker disse i funksjonene addisjon,
    subtraksjon og divisjon. Spør så bruker om et antall tommer,
    bruker dette til å printe ut antallet i cm ved hjelp av tommerTilCm
    """
    tall1 = float(input("Tall1: "))
    tall2 = float(input("Tall2: "))

    print("%.2f + %.2f = %.2f" % (tall1, tall2, addisjon(tall1, tall2)))
    print("%.2f - %.2f = %.2f" % (tall1, tall2, subtraksjon(tall1, tall2)))
    print("%.2f / %.2f = %.2f\n" % (tall1, tall2, divisjon(tall1, tall2)))

    tommer = float(input("Tommer: "))
    print("%.2f\" = %.2fcm" % (tommer, tommerTilCm(tommer)))

# Definerer hovedprogrammet
def main():
    # Tester addisjon, subtraksjon og divisjon:
    addisjonTest()
    subtraksjonTest()
    divisjonTest()

    # Tester funksjonene
    print("Funksjonstest: ")
    print("21 + 21 =", addisjon(21, 21))
    print("12\" = %scm" % tommerTilCm(12))
    print("----")
    # Kjører prosedyren skrivBeregninger
    skrivBeregninger()

# Kjører hovedprogrammet
main()
