# Definerer gjennomsnittListe
def gjennomsnittListe(liste):
    """Returnerer gjenomsnittet av tallene i en liste"""
    listeSum = 0
    for i in liste:
        listeSum += i
    gjennomsnitt = listeSum / len(liste)
    return gjennomsnitt

# Definerer hovedprogrammet
def main():
    # Lager en tom liste tempListe
    tempListe = []
    # legger hver linje i temperatur.txt til i tempListe som et heltall.
    with open("temperatur.txt", "r") as f:
        for line in f:
            tempListe.append(int(line.strip("\n")))
    # Bruker gjennomsnittListe() til å printe snittet fra temperatur.txt
    snitt = gjennomsnittListe(tempListe)
    print("Gjennomsnittet av verdiene i temperatur.txt er:", snitt)

# Kjører hovedprogrammet
main()
