# definerer slaaSammen som konkattinerer to strenger
def slaaSammen(str1, str2):
    return str1 + str2

# Definerer skrivUt som printer ut elementene i en liste
def skrivUt(liste):
    print("\nI Listen er følgende: ")
    for i in liste:
        print(i)
    print()

# Definerer hovedprogrammet
def main():
    # Lager en tom liste mineOrd
    mineOrd = []

    # Lager en boolean fortsetter, settes til True
    fortsetter = True
    # Mens fortsetter er sant, kjører while løkken.
    while fortsetter:
        # Spør om input fra brukeren
        brukerInput = input("\"i\" for input, \"u\" for utskrift, \"s\" for å avslutte\n").lower()

        # Dersom inputten er "i", spør programmet brukeren om to strenger
        # Disse konkattineres ved bruk av slaaSammen, og legges til i mineOrd
        if brukerInput == "i":
            str1 = input("str1 = ")
            str2 = input("str2 = ")
            mineOrd.append(slaaSammen(str1, str2))

        # Dersom inputten er "u", brukes skrivUt til å printe ut ordene i mineOrd
        elif brukerInput == "u":
            skrivUt(mineOrd)
        # Dersom inputten er "s", settes fortsetter til false, og løkken slutter
        elif brukerInput == "s":
            fortsetter = False
        # Dersom ingen av kommandoene blir brukt, bes bruker om å prøve på nytt.
        else:
            print("Prøv igjen")

# Kjører hovedprogrammet
main()
