# Prosedyre som tar input alder fra bruker, bruker så alder til å avgjøre
# hvor mye bruker må betale for en billett, dette printes så ut.
# Dersom bruker ikke skriver inn et tall, kjøres prosedyren på nytt (rekursivt?)

def pris():
    try:
        alder = int(input("Skriv inn din alder: "))
        billettPris = 0

        if alder <= 17:
            billettPris = 30
        elif alder >= 63:
            billettPris = 35
        else:
            billettPris = 50
        print("Du må betale %ikr for en billett!" % billettPris)

    except ValueError:
        print("Det er ikke et tall! Prøv igjen.")
        pris()
    print()

# Finner ikke noen åpenbare problemer med prosedyren slik jeg har skrevet den.
# Vil tro hvis man lager if setningene i den samme rekkefølgen som i oppgaven, så ville
# problemet vært at den aldri ville sjekket om brukeren er 63 år eller eldre.

# kaller prosedyren 4 ganger
pris()
pris()
pris()
pris()
