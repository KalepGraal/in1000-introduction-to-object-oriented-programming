# Definerer en liste med tre heltall, legger så til enda ett heltall
liste = [3, 5, 2]
liste.append(4)

# Printer første og tredje element i lista
print("\nForste element i lista:", liste[0])
print("Tredje element i lista:", liste[2],"\n")

# Definerer variablen mittNavn
mittNavn = "cf"

# Lager en tom liste navn
navn = []

# Kjører koden 4 ganger:
    # Spør brukeren om et navn, legger den til listen navn
for i in range(4):
    inp = input("Oppgi et navn: ").lower()
    navn.append(inp)

# Printer et svar ut ifra om mittNavn er i listen navn.
if mittNavn in navn:
    print("\nDu husket meg!\n")
else:
    print("\nGlemte du meg?\n")

# Lager en ny liste som består av liste og navn, printes så ut.
nyListe = liste + navn
print(nyListe)

# Fjerner siste element i nyListe (2 ganger)
nyListe.pop()
nyListe.pop()

# Printer så nyListe ut igjen.
print(nyListe)
