# Bruker modulen ezgraphics til å tegne en rød sirkel

from ezgraphics import GraphicsWindow

vindu = GraphicsWindow(200,200)
lerret = vindu.canvas()

lerret.setColor("red")
lerret.drawOval(50,50,100,100)

vindu.wait()
