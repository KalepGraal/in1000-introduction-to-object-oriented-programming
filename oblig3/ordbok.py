# Definerer varer som en ordbok med ulike varer, printes så ut.
varer = {"melk":14.90, "brød":24.90, "yoghurt":12.90, "pizza":39.90}
print(varer)

# Lager en prosedyre for å legge til en ny vare til varer.
def leggeTilVare():
    """Legger til en ny vare til ordboken varer"""
    # Spør bruker om navn på vare
    varenavn = input("Navn på vare: ").lower()
    # Bruker try/except for å sikre at bruker skriver inn et tall som pris
    try:
        # spør bruker om pris på vare
        pris = float(input("Pris på vare: "))
        # Legger vare og pris til i orboken varer
        varer[varenavn] = pris

    # Dette kjører dersom brukeren ikke skriver inn et tall.
    except ValueError:
        print("Ikke et tall, prøv igjen!")
        # Starter prosedyren på nytt.
        leggeTilVare()

# Kaller prosedyren to ganger.
leggeTilVare()
leggeTilVare()


# Printer ut ordboken i et lesbart format.
print()
for i in varer:
    print("%9s koster: %6.2fkr" % (i, varer[i]))
