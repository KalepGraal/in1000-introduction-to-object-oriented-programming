""" Oppgave: Skriv et program som tar imot koordinater samt høyde og bredde fra
bruker, legger disse etter hverandre i en liste og deretter bruker innholdet i
listen til å tegne opp en form med EzGraphics. """

# Bruker modulen ezgraphics til å tegne en grønn firkant utifra brukerinput
from ezgraphics import GraphicsWindow

print("Skriv inn koordinater og størrelse på en firkant")
firkant = []

# Bruker try for å forsikre meg om at bruker skriver inn heltall.
try:
    # Legger variablene til i listen firkant.
    firkant.append(int(input("x: ")))
    firkant.append(int(input("y: ")))
    firkant.append(int(input("Høyde: ")))
    firkant.append(int(input("Bredde: ")))

    # Starter det grafiske vinduet
    win = GraphicsWindow()
    canvas = win.canvas()
    win.setTitle("Koordinater")

    # Setter fargen til grønn, og lager firkanten med variablene i firkant.
    canvas.setColor("Green")
    canvas.drawRect(firkant[0], firkant[1], firkant[2], firkant[3])

    win.wait()

except ValueError:
    print("Error, skriv kun inn heltall!")
