# Oppgave 5: Skriv et program som lager en handleliste ved hjelp av løkker og lister.

# Lager en liste handleliste
handleliste = []

# Printer info til brukeren
print("Skriv inn varer du vil ha i handlelisten, skriv inn 0 som varenavn for å avslutte, og printe handlelista")
# Ber om navn på vare som bruker skal legge til i lista
vareNavn = input("Varenavn: ")

# Så lenge varenavnet ikke er 0, så vil den fortsette å spørre om varenavn, og
# antallet, for så å legge de til i handlelista.
while vareNavn != "0":
    vare = []

    if vareNavn != "0":
        vare.append(vareNavn)
        vare.append(input("Antall: "))
        handleliste.append(vare)

    print(vareNavn, "lagt til i lista (tast 0 for å avslutte)")
    vareNavn = input("Varenavn: ")

# Printer ut alle elementene i handlelista.
print("\n--- Handleliste ---")
for i in range(len(handleliste)): # [0, 1]
    print("%sx %s" %(handleliste[i][1], handleliste[i][0]))
