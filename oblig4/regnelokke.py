# Definerer en tom liste, tallListe
tallListe = []

# Ber om et tall fra bruker, helt til bruker taster inn 0.
# Legger tallene til i lista tallListe
tall = int(input("Tast inn et tall (0 to stop): "))
while tall != 0:
    tallListe.append(tall)
    tall = int(input("Tast inn et tall (0 to stop): "))

# Printer alle tallene i lista tallListe
for i in range(len(tallListe)):
    print("tall %i = %s" % (i+1, tallListe[i]))

# Summerer alle tallene i lista, og printer det ut.
minSum = 0
for i in tallListe:
    minSum += i
print("Summen av tallene er %i!" % minSum)

# Finner minste verdi i tallListe, og printer dette ut.
minsteVerdi = tallListe[0] ##
for i in tallListe:
    if i < minsteVerdi:
        minsteVerdi = i
print("Minste verdi er", minsteVerdi)

# Finner største verdi i tallListe, og printer dette ut.
storsteVerdi = tallListe[0] ##
for i in tallListe:
    if i > storsteVerdi:
        storsteVerdi = i
print("Største verdi er", storsteVerdi)
