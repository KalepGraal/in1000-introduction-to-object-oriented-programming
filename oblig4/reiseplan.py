# Lager tre lister: steder, klesplag og avreisedatoer
steder = []
klesplagg = []
avreisedatoer = []

# Lar bruker skrive inn fem elementer til hver liste
for i in range(5):
    steder.append(input("Skriv et reisemål: "))
    klesplagg.append(input("Skriv et klesplagg: "))
    avreisedatoer.append(input("Skriv avreisedato: "))

# Legger de tre listene inn i en liste reiseplan.
reiseplan = [steder, klesplagg, avreisedatoer]

# Printer ut listene i reiseplan
for i in reiseplan:
    print(i)

# Ber bruker om to indekser
i1 = int(input("steder(0), klesplagg(1), avreisedato(2) = "))
i2 = int(input("index2 = "))

# Sjekker at indeksene er gyldige plasseringer i den nøstede listen.
# Printer så ut elementet som har denne plasseringen.
if i1 in range(len(reiseplan)) and i2 in range(len(reiseplan[0])):
    print(reiseplan[i1][i2])
# Dersom indeksene ikke er gyldige plasseringer, får bruker beskjed om det.
else:
    print("Ugyldig input!")
