# Definerer en funksjon adder, som tar to parametre, tall1 og tall2.
# Returnerer summen av disse.
def adder(tall1, tall2):
    return tall1 + tall2

# Tester funksjonen to ganger.
print("3 + 4 =", adder(3,4))
print("21 + 64 =", adder(21,64))

# Definerer en funksjon som tar to parametre, minTekst og minBokstav.
# Returnerer antall ganger minBokstav forekommer i minTekst.
def tellForekomst(minTekst, minBokstav):
    bokstaverITekst = 0
    for i in range(len(minTekst)):
        if minTekst[i] == bokstav:
            bokstaverITekst += 1
    return bokstaverITekst

# Skaffer brukerinput
tekst = input("Skriv litt tekst: ")
bokstav = input("Skriv en bokstav: ")

# Bruker brukerinputten i funksjonen tellForekomst.
print("Bokstaven \"%s\" forekommer %i ganger i teksten." % (bokstav, tellForekomst(tekst, bokstav)))
