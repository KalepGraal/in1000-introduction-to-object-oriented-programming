# Importerer klassen GraphicsWindow fra ezgraphics
from ezgraphics import GraphicsWindow

# Lager et grafikkvindu, og et lerret til grafikkvinduet
win = GraphicsWindow()
can = win.canvas()

# Definerer variablene teller, x_pos, og stoerrelse.
teller = 0
x_pos = 10
stoerrelse = 50

# Kjører 9 ganger:
#    tegner en sirkel som øker i størrelse for hver iterasjon
#    incrementer variablene teller, x_pos, og stoerrelse.
while teller < 9:
    can.drawOval(x_pos, 100, stoerrelse, stoerrelse)
    x_pos += 35
    stoerrelse += 5
    teller += 1

# Forteller vinduet at det ikke lukkes før brukeren gjør det.
win.wait()
