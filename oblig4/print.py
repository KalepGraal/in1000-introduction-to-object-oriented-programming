## - Oppgave 1
# Definerer en funksjon adder, som tar to parametre, tall1 og tall2.
# Returnerer summen av disse.
def adder(tall1, tall2):
    return tall1 + tall2

# Tester funksjonen to ganger.
print("3 + 4 =", adder(3,4))
print("21 + 64 =", adder(21,64))

# Definerer en funksjon som tar to parametre, minTekst og minBokstav.
# Returnerer antall ganger minBokstav forekommer i minTekst.
def tellForekomst(minTekst, minBokstav):
    bokstaverITekst = 0
    for i in range(len(minTekst)):
        if minTekst[i] == bokstav:
            bokstaverITekst += 1
    return bokstaverITekst

# Skaffer brukerinput
tekst = input("Skriv litt tekst: ")
bokstav = input("Skriv en bokstav: ")

# Bruker brukerinputten i funksjonen tellForekomst.
print("Bokstaven \"%s\" forekommer %i ganger i teksten." % (bokstav, tellForekomst(tekst, bokstav)))


## - Oppgave 2
# Definerer en tom liste, tallListe
tallListe = []

# Ber om et tall fra bruker, helt til bruker taster inn 0.
# Legger tallene til i lista tallListe
tall = int(input("Tast inn et tall (0 to stop): "))
while tall != 0:
    tallListe.append(tall)
    tall = int(input("Tast inn et tall (0 to stop): "))

# Printer alle tallene i lista tallListe
for i in range(len(tallListe)):
    print("tall %i = %s" % (i+1, tallListe[i]))

# Summerer alle tallene i lista, og printer det ut.
minSum = 0
for i in tallListe:
    minSum += i
print("Summen av tallene er %i!" % minSum)

# Finner minste verdi i tallListe, og printer dette ut.
minsteVerdi = tallListe[0] ##
for i in tallListe:
    if i < minsteVerdi:
        minsteVerdi = i
print("Minste verdi er", minsteVerdi)

# Finner største verdi i tallListe, og printer dette ut.
storsteVerdi = tallListe[0] ##
for i in tallListe:
    if i > storsteVerdi:
        storsteVerdi = i
print("Største verdi er", storsteVerdi)

## - Oppgave 3
# Importerer klassen GraphicsWindow fra ezgraphics
from ezgraphics import GraphicsWindow

# Lager et grafikkvindu, og et lerret til grafikkvinduet
win = GraphicsWindow()
can = win.canvas()

# Definerer variablene teller, x_pos, og stoerrelse.
teller = 0
x_pos = 10
stoerrelse = 50

# Kjører 9 ganger:
#    tegner en sirkel som øker i størrelse for hver iterasjon
#    incrementer variablene teller, x_pos, og stoerrelse.
while teller < 9:
    can.drawOval(x_pos, 100, stoerrelse, stoerrelse)
    x_pos += 35
    stoerrelse += 5
    teller += 1

# Forteller vinduet at det ikke lukkes før brukeren gjør det.
win.wait()

## - Oppgave 4
# Lager tre lister: steder, klesplag og avreisedatoer
steder = []
klesplagg = []
avreisedatoer = []

# Lar bruker skrive inn fem elementer til hver liste
for i in range(5):
    steder.append(input("Skriv et reisemål: "))
    klesplagg.append(input("Skriv et klesplagg: "))
    avreisedatoer.append(input("Skriv avreisedato: "))

# Legger de tre listene inn i en liste reiseplan.
reiseplan = [steder, klesplagg, avreisedatoer]

# Printer ut listene i reiseplan
for i in reiseplan:
    print(i)

# Ber bruker om to indekser
i1 = int(input("steder(0), klesplagg(1), avreisedato(2) = "))
i2 = int(input("index2 = "))

# Sjekker at indeksene er gyldige plasseringer i den nøstede listen.
# Printer så ut elementet som har denne plasseringen.
if i1 in range(len(reiseplan)) and i2 in range(len(reiseplan[0])):
    print(reiseplan[i1][i2])
# Dersom indeksene ikke er gyldige plasseringer, får bruker beskjed om det.
else:
    print("Ugyldig input!")




















## - Oppgave 5
# Oppgave 5: Skriv et program som lager en handleliste ved hjelp av løkker og lister.

# Lager en liste handleliste
handleliste = []

# Printer info til brukeren
print("Skriv inn varer du vil ha i handlelisten, skriv inn 0 som varenavn for å avslutte, og printe handlelista")
# Ber om navn på vare som bruker skal legge til i lista
vareNavn = input("Varenavn: ")

# Så lenge varenavnet ikke er 0, så vil den fortsette å spørre om varenavn, og
# antallet, for så å legge de til i handlelista.
while vareNavn != "0":
    vare = []

    if vareNavn != "0":
        vare.append(vareNavn)
        vare.append(input("Antall: "))
        handleliste.append(vare)

    print(vareNavn, "lagt til i lista (tast 0 for å avslutte)")
    vareNavn = input("Varenavn: ")

# Printer ut alle elementene i handlelista.
print("\n--- Handleliste ---")
for i in range(len(handleliste)): # [0, 1]
    print("%sx %s" %(handleliste[i][1], handleliste[i][0]))
